import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ToastController } from '@ionic/angular';
import { LoginService } from '../service/login.service';

@Component({
  selector: 'app-forgot',
  templateUrl: './forgot.page.html',
  styleUrls: ['./forgot.page.scss'],
})
export class ForgotPage implements OnInit {
  forgotForm: FormGroup;

  constructor(
    private builder: FormBuilder,    
    private toast: ToastController,
    private service: LoginService
  ) { }

  ngOnInit() {
    this.forgotForm = this.builder.group({
      email: ['', [Validators.required, Validators.email]]
    })
  }

  async forgot(){ 
    const email = this.forgotForm.value;
    this.service.forgot(email.email);
    const ctrl = await this.toast.create({
      message: 'Mensagem Enviada',
      duration: 3000
    });
    ctrl.present();
  }

}
