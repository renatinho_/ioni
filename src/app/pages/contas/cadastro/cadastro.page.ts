import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { NavController } from '@ionic/angular';
import { ContaService } from '../service/conta-service';

@Component({
  selector: 'app-cadastro',
  templateUrl: './cadastro.page.html',
  styleUrls: ['./cadastro.page.scss'],
})
export class CadastroPage implements OnInit {
  contasForm: FormGroup;
  
  
  constructor(
    private builder: FormBuilder,
    private nav: NavController,
    private conta: ContaService
  ) { }

  
  ngOnInit() {
    this.initForm();
  }

  private initForm(){ // Inicia o formulário na página
    this.contasForm = this.builder.group({
      valor: ['', [Validators.required, Validators.min(0.01)]],
      parceiro:['', [Validators.required, Validators.minLength(5)]],
      descricao: ['', [Validators.required, Validators.minLength(6)]],
      tipo: ['', Validators.required]
    });
  }

  registraConta(){ // Registra a nova conta no Firebase
    const data = this.contasForm.value;
    this.conta.registraConta(data).
    then(() => this.nav.navigateForward('contas/pagar'));
  }

}
