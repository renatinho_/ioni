// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig: {
    apiKey: 'AIzaSyCE6SfW_PExhwS7KuTI0rLuaXH8CxlN2XE',
    authDomain: 'mcontabilidade-if.firebaseapp.com',
    databaseURL: 'https://mcontabilidade-if.firebaseio.com',
    projectId: 'mcontabilidade-if',
    storageBucket: 'mcontabilidade-if.appspot.com',
    messagingSenderId: '788821741313',
    appId: '1:788821741313:web:c35d5cc9be9fc75e281500',
    measurementId: 'G-T5JM8J9K3Y'
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
