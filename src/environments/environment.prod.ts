export const environment = {
  production: true,
  firebaseConfig: {
    apiKey: 'AIzaSyCE6SfW_PExhwS7KuTI0rLuaXH8CxlN2XE',
    authDomain: 'mcontabilidade-if.firebaseapp.com',
    databaseURL: 'https://mcontabilidade-if.firebaseio.com',
    projectId: 'mcontabilidade-if',
    storageBucket: 'mcontabilidade-if.appspot.com',
    messagingSenderId: '788821741313',
    appId: '1:788821741313:web:c35d5cc9be9fc75e281500',
    measurementId: 'G-T5JM8J9K3Y'
  }
};
